﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Bookshop.Models
{
    public partial class OrderAuthor
    {
        public int Book { get; set; }
        public int Author { get; set; }
        public int Quantity { get; set; }
        public int Id { get; set; }
        public int? Purchased { get; set; }

        public virtual Author AuthorNavigation { get; set; }
        public virtual Book BookNavigation { get; set; }
    }
}
