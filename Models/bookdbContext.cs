﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Bookshop.Models
{
    public partial class bookdbContext : DbContext
    {
        public bookdbContext()
        {
        }

        public bookdbContext(DbContextOptions<bookdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Administrator> Administrators { get; set; }
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Catalog> Catalogs { get; set; }
        public virtual DbSet<Costumer> Costumers { get; set; }
        public virtual DbSet<Genre> Genres { get; set; }
        public virtual DbSet<OrderAuthor> OrderAuthors { get; set; }
        public virtual DbSet<OrderCostumer> OrderCostumers { get; set; }
        public virtual DbSet<Publisher> Publishers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-FLUE5KV\\ROOT;Database=bookdb;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Administrator>(entity =>
            {
                entity.ToTable("administrator", "bookdb");

                entity.HasIndex(e => e.Catalog, "adm_catalog_idx");

                entity.HasIndex(e => e.Id, "administrator$ID_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Username, "administrator$username_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("ID");

                entity.Property(e => e.Catalog).HasColumnName("catalog");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("email");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasColumnName("password");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasColumnName("username");

                entity.HasOne(d => d.CatalogNavigation)
                    .WithMany(p => p.Administrators)
                    .HasForeignKey(d => d.Catalog)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("administrator$adm_catalog");
            });

            modelBuilder.Entity<Author>(entity =>
            {
                entity.ToTable("author", "bookdb");

                entity.HasIndex(e => e.Id, "author$ID_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Email, "author$email_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Username, "author$username_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Adm, "author_adm_idx");

                entity.HasIndex(e => e.Catalog, "author_catalog_idx");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Adm).HasColumnName("adm");

                entity.Property(e => e.Catalog).HasColumnName("catalog");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("email");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasColumnName("password");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("username");

                entity.HasOne(d => d.AdmNavigation)
                    .WithMany(p => p.Authors)
                    .HasForeignKey(d => d.Adm)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("author$author_adm");

                entity.HasOne(d => d.CatalogNavigation)
                    .WithMany(p => p.Authors)
                    .HasForeignKey(d => d.Catalog)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("author$author_catalog");
            });

            modelBuilder.Entity<Book>(entity =>
            {
                entity.ToTable("book", "bookdb");

                entity.HasIndex(e => e.Id, "book$ID_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Author, "book_author_idx");

                entity.HasIndex(e => e.Catalog, "book_catalog_idx");

                entity.HasIndex(e => e.Genre, "book_genre_idx");

                entity.HasIndex(e => e.Publisher, "book_publisher_idx");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("ID");

                entity.Property(e => e.Author).HasColumnName("author");

                entity.Property(e => e.Catalog).HasColumnName("catalog");

                entity.Property(e => e.CoverImage)
                    .HasMaxLength(45)
                    .HasColumnName("cover_image");

                entity.Property(e => e.Genre).HasColumnName("genre");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Publisher).HasColumnName("publisher");

                entity.Property(e => e.Rating).HasColumnName("rating");

                entity.Property(e => e.RatingGoodreads).HasColumnName("rating_goodreads");

                entity.Property(e => e.Stock).HasColumnName("stock");

                entity.Property(e => e.Summary).HasColumnName("summary");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("title");

                entity.Property(e => e.Voted).HasColumnName("voted");

                entity.Property(e => e.Year).HasColumnName("year");

                entity.HasOne(d => d.AuthorNavigation)
                    .WithMany(p => p.Books)
                    .HasForeignKey(d => d.Author)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("book$book_author");

                entity.HasOne(d => d.CatalogNavigation)
                    .WithMany(p => p.Books)
                    .HasForeignKey(d => d.Catalog)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("book$book_catalog");

                entity.HasOne(d => d.GenreNavigation)
                    .WithMany(p => p.Books)
                    .HasForeignKey(d => d.Genre)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("book$book_genre");

                entity.HasOne(d => d.PublisherNavigation)
                    .WithMany(p => p.Books)
                    .HasForeignKey(d => d.Publisher)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("book$book_publisher");
            });

            modelBuilder.Entity<Catalog>(entity =>
            {
                entity.HasKey(e => e.Catalog1)
                    .HasName("PK_catalog_catalog");

                entity.ToTable("catalog", "bookdb");

                entity.HasIndex(e => e.Catalog1, "catalog$ID_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Catalog1)
                    .ValueGeneratedNever()
                    .HasColumnName("catalog");
            });

            modelBuilder.Entity<Costumer>(entity =>
            {
                entity.ToTable("costumer", "bookdb");

                entity.HasIndex(e => e.Id, "costumer$ID_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Email, "costumer$email_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Username, "costumer$username_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Catalog, "iser_catalog_idx");

                entity.HasIndex(e => e.Adm, "user_adm_idx");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Adm).HasColumnName("adm");

                entity.Property(e => e.Catalog).HasColumnName("catalog");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("email");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasColumnName("password");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasColumnName("username");

                entity.HasOne(d => d.AdmNavigation)
                    .WithMany(p => p.Costumers)
                    .HasForeignKey(d => d.Adm)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("costumer$user_adm");

                entity.HasOne(d => d.CatalogNavigation)
                    .WithMany(p => p.Costumers)
                    .HasForeignKey(d => d.Catalog)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("costumer$iser_catalog");
            });

            modelBuilder.Entity<Genre>(entity =>
            {
                entity.ToTable("genre", "bookdb");

                entity.HasIndex(e => e.Id, "genre$ID_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "genre$name_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<OrderAuthor>(entity =>
            {
                entity.ToTable("order_author", "bookdb");

                entity.HasIndex(e => e.Id, "order_author$id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Author, "order_author_author_idx");

                entity.HasIndex(e => e.Book, "order_author_book_idx");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("ID");

                entity.Property(e => e.Author).HasColumnName("author");

                entity.Property(e => e.Book).HasColumnName("book");

                entity.Property(e => e.Purchased).HasColumnName("purchased");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.AuthorNavigation)
                    .WithMany(p => p.OrderAuthors)
                    .HasForeignKey(d => d.Author)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("order_author$order_author_author");

                entity.HasOne(d => d.BookNavigation)
                    .WithMany(p => p.OrderAuthors)
                    .HasForeignKey(d => d.Book)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("order_author$order_author_book");
            });

            modelBuilder.Entity<OrderCostumer>(entity =>
            {
                entity.ToTable("order_costumer", "bookdb");

                entity.HasIndex(e => e.Id, "order_costumer$id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Book, "order_costumer_book_idx");

                entity.HasIndex(e => e.Costumer, "order_costumer_costumer_idx");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("ID");

                entity.Property(e => e.Book).HasColumnName("book");

                entity.Property(e => e.Costumer).HasColumnName("costumer");

                entity.Property(e => e.Purchased).HasColumnName("purchased");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.BookNavigation)
                    .WithMany(p => p.OrderCostumers)
                    .HasForeignKey(d => d.Book)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("order_costumer$order_costumer_book");

                entity.HasOne(d => d.CostumerNavigation)
                    .WithMany(p => p.OrderCostumers)
                    .HasForeignKey(d => d.Costumer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("order_costumer$order_costumer_costumer");
            });

            modelBuilder.Entity<Publisher>(entity =>
            {
                entity.ToTable("publisher", "bookdb");

                entity.HasIndex(e => e.Id, "publisher$ID_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Email, "publisher$email_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Username, "publisher$username_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Adm, "publisher_adm_idx");

                entity.HasIndex(e => e.Catalog, "publisher_catalog_idx");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Adm).HasColumnName("adm");

                entity.Property(e => e.Catalog).HasColumnName("catalog");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("email");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasColumnName("password");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasColumnName("username");

                entity.HasOne(d => d.AdmNavigation)
                    .WithMany(p => p.Publishers)
                    .HasForeignKey(d => d.Adm)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("publisher$publisher_adm");

                entity.HasOne(d => d.CatalogNavigation)
                    .WithMany(p => p.Publishers)
                    .HasForeignKey(d => d.Catalog)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("publisher$publisher_catalog");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
