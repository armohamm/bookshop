﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Bookshop.Models
{
    public partial class Catalog
    {
        public Catalog()
        {
            Administrators = new HashSet<Administrator>();
            Authors = new HashSet<Author>();
            Books = new HashSet<Book>();
            Costumers = new HashSet<Costumer>();
            Publishers = new HashSet<Publisher>();
        }

        public int Catalog1 { get; set; }

        public virtual ICollection<Administrator> Administrators { get; set; }
        public virtual ICollection<Author> Authors { get; set; }
        public virtual ICollection<Book> Books { get; set; }
        public virtual ICollection<Costumer> Costumers { get; set; }
        public virtual ICollection<Publisher> Publishers { get; set; }
    }
}
