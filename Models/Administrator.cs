﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Bookshop.Models
{
    public partial class Administrator
    {
        public Administrator()
        {
            Authors = new HashSet<Author>();
            Costumers = new HashSet<Costumer>();
            Publishers = new HashSet<Publisher>();
        }

        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Catalog { get; set; }
        public int Id { get; set; }

        public virtual Catalog CatalogNavigation { get; set; }
        public virtual ICollection<Author> Authors { get; set; }
        public virtual ICollection<Costumer> Costumers { get; set; }
        public virtual ICollection<Publisher> Publishers { get; set; }
    }
}
