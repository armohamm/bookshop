﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Bookshop.Models
{
    public partial class OrderCostumer
    {
        public int Book { get; set; }
        public int Costumer { get; set; }
        public int Quantity { get; set; }
        public int Id { get; set; }
        public int? Purchased { get; set; }

        public virtual Book BookNavigation { get; set; }
        public virtual Costumer CostumerNavigation { get; set; }
    }
}
