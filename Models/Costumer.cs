﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Bookshop.Models
{
    public partial class Costumer
    {
        public Costumer()
        {
            OrderCostumers = new HashSet<OrderCostumer>();
        }

        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Adm { get; set; }
        public int Catalog { get; set; }
        public int Id { get; set; }

        public virtual Administrator AdmNavigation { get; set; }
        public virtual Catalog CatalogNavigation { get; set; }
        public virtual ICollection<OrderCostumer> OrderCostumers { get; set; }
    }
}
