﻿using System;
using System.Collections.Generic;
using System.Linq;

#nullable disable

namespace Bookshop.Models
{
    public partial class Genre 
    {
        public Genre()
        {
            Books = new HashSet<Book>();
        }

        public string Name { get; set; }
        public int Id { get; set; }

        public virtual ICollection<Book> Books { get; set; }

        private List<IObserver> _observers = new List<IObserver>();
        //attach observer book to genre
        internal void Attach(IObserver observer)
        {
            Console.WriteLine("Genre: Attached an observer book.");
            _observers.Add(observer);
        }
        //detach observer book from genre
        internal void Detach(IObserver observer)
        {
            _observers.Remove(observer);
            Console.WriteLine("Genre: Detached an observer book.");
        }
        //notify all observer books that genre name changed
        internal void Notify(IQueryable<IObserver> observers)
        {
            Console.WriteLine("Genre: Notifying observer books...");

            foreach (var observer in observers)
            {
                observer.Update();
            }
        }
    }
}
