﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Bookshop.Models
{
    public partial class Book : IObserver
    {
        public Book()
        {
            OrderAuthors = new HashSet<OrderAuthor>();
            OrderCostumers = new HashSet<OrderCostumer>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string CoverImage { get; set; }
        public string Summary { get; set; }
        public double Price { get; set; }
        public double RatingGoodreads { get; set; }
        public double? Rating { get; set; }
        public int Genre { get; set; }
        public int Author { get; set; }
        public int Publisher { get; set; }
        public int? Year { get; set; }
        public int Catalog { get; set; }
        public int? Stock { get; set; }
        public int? Voted { get; set; }

        public virtual Author AuthorNavigation { get; set; }
        public virtual Catalog CatalogNavigation { get; set; }
        public virtual Genre GenreNavigation { get; set; }
        public virtual Publisher PublisherNavigation { get; set; }
        public virtual ICollection<OrderAuthor> OrderAuthors { get; set; }
        public virtual ICollection<OrderCostumer> OrderCostumers { get; set; }
        //Update method from IObserver interface
        public void Update()
        {
            Console.WriteLine("Book "+Title+": Changed genre name.");
        }
    }
}
