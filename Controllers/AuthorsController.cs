﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Bookshop.Models;

namespace Bookshop.Controllers
{
    public class AuthorsController : Controller
    {
        private readonly bookdbContext _context;

        public AuthorsController(bookdbContext context)
        {
            _context = context;
        }

        // GET: Author authentification
        public async Task<IActionResult> Index(string username, string password)
        {
            var authors = from m in _context.Authors.Include(a => a.AdmNavigation).Include(a => a.CatalogNavigation)
                          select m;
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                authors = authors.Where(s => s.Username.Contains(username))
                    .Where(s => s.Password.Contains(password));
            }
            else return UserNotFound();
            if (!authors.Any()) return UserNotFound();
            return View(await authors.ToListAsync());
        }

        public IActionResult UserNotFound()
        {
            return View("UserNotFound");
        }
        //get books results from search or just all the books
        public async Task<IActionResult> Catalog(int? id, string genre, string titleSearch, string author)
        {
            ViewData["ID"] = id;
            ViewData["titleSearch"] = titleSearch;
            ViewData["author"] = author;
            ViewData["genre"] = genre;
            var bookdbContext = from m in _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .OrderByDescending(book => book.RatingGoodreads)
                select m;

            if (!String.IsNullOrEmpty(genre))
            {
                bookdbContext = bookdbContext.Where(s => s.GenreNavigation.Name.Contains(genre));
            }
            if (!String.IsNullOrEmpty(titleSearch))
            {
                bookdbContext = bookdbContext.Where(s => s.Title.Contains(titleSearch));
            }
            if (!String.IsNullOrEmpty(author))
            {
                bookdbContext = bookdbContext.Where(s => s.AuthorNavigation.Username.Contains(author));
            }

            return View(await bookdbContext.ToListAsync());
        }

        // GET: author detail
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var author = await _context.Authors
                .Include(a => a.AdmNavigation)
                .Include(a => a.CatalogNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (author == null)
            {
                return NotFound();
            }

            return View(author);
        }
        //get create book information
        public IActionResult CreateBook(int? id)
        {
            ViewData["ID"] = id;
            ViewData["idBook"] = _context.Books.OrderBy(book => book.Id).Last().Id + 1;
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Username");
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1");
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name");
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Username");
            return View();
        }

        // POST: submit changes to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateBook([Bind("Id,Title,CoverImage,Summary,Price,RatingGoodreads,Rating,Genre,Author,Publisher,Year,Catalog,Stock,Voted")] Book book, int? userId)
        {
            if (ModelState.IsValid)
            {


                _context.Add(book);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Catalog), new { id = userId });
            }
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Email", book.Author);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", book.Catalog);
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name", book.Genre);
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Email", book.Publisher);
            return View(book);
        }
        //get book to edit
        public async Task<IActionResult> EditBook(int? id, int? userId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }
            ViewData["ID"] = userId;

            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Email", book.Author);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", book.Catalog);
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name", book.Genre);
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Email", book.Publisher);
            return View(book);
        }

        // POST: submit changed book to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBook(int id, [Bind("Id,Title,CoverImage,Summary,Price,RatingGoodreads,Rating,Genre,Author,Publisher,Year,Catalog,Stock,Voted")] Book book, int? userId)
        {
            if (id != book.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Catalog), new { id = userId });
            }
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Email", book.Author);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", book.Catalog);
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name", book.Genre);
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Email", book.Publisher);
            return View(book);
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.Id == id);
        }

        // GET: book details
        public async Task<IActionResult> DetailsBook(int? id, int? userId, int? rate)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            //authors can vote for the book rating
            if (rate.HasValue)
            {
                if (book.Rating == null)
                {
                    book.Rating = rate;
                    book.Voted++;
                }
                else
                {
                    book.Rating = (book.Rating * book.Voted) + rate;
                    book.Voted++;
                    book.Rating /= book.Voted;
                }
                _context.Update(book);
                await _context.SaveChangesAsync();
            }
            //format the rating to what we want to see
            if (book.Rating == null) ViewData["Rating"] = "not voted yet";
            else
            {
                var r = Convert.ToDouble(String.Format("{0:0.00}", book.Rating));
                ViewData["Rating"] = r;
            }
            if (book == null)
            {
                return NotFound();
            }
            ViewData["ID"] = userId;
            ViewData["bookId"] = id;
            return View(book);
        }

        // GET: Book to be deleted
        public async Task<IActionResult> DeleteBook(int? id, int? userId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }
            if (book.Rating == null) ViewData["Rating"] = "not voted yet";
            else
            {
                var r = Convert.ToDouble(String.Format("{0:0.00}", book.Rating));
                ViewData["Rating"] = r;
            }
            ViewData["ID"] = userId;
            return View(book);
        }

        // POST: delete book from db
        [HttpPost, ActionName("DeleteBook")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmedBook(int id, int? userId)
        {
            var book = await _context.Books.FindAsync(id);
            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Catalog), new { id = userId });
        }
        //get the books written by the author
        public async Task<IActionResult> MyBooks(int? id)
        {
            var books = from m in _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Where(b => b.AuthorNavigation.Id == id)
                        select m;

            ViewData["ID"] = id;
            return View(await books.OrderByDescending(book => book.RatingGoodreads).ToListAsync());
        }
        //get author cart
        public async Task<IActionResult> Orders(int? id)
        {
            var bookdbContext = _context.OrderAuthors
                .Include(o => o.BookNavigation)
                .Include(o => o.AuthorNavigation)
                .Where(o => o.AuthorNavigation.Id == id)
                .Where(o => o.Purchased == 0);
            var books = bookdbContext.ToList();
            ViewData["ID"] = id;
            double totalPrice = 0;
            //calculate total price
            for (int i = 0; i < books.Count(); i++)
            {
                var order = books.ElementAt(i);
                totalPrice += (order.BookNavigation.Price * order.Quantity);
            }
            ViewData["Price"] = totalPrice;
            return View(await bookdbContext.ToListAsync());
        }
        //purchase books from cart, change books stock and add them to previous orders
        public async Task<IActionResult> Purchase(int? id)
        {
            var bookdbContext = _context.OrderAuthors
                .Include(o => o.BookNavigation)
                .Include(o => o.AuthorNavigation)
                .Where(o => o.AuthorNavigation.Id == id)
                .Where(o => o.Purchased == 0)
                .ToList();
            for (int i = 0; i < bookdbContext.Count(); i++)
            {
                var order = bookdbContext.ElementAt(i);
                order.Purchased = 1;
                _context.Update(order);
                var book = await _context.Books.FindAsync(order.BookNavigation.Id);
                book.Stock -= order.Quantity;
                _context.Update(book);
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Catalog), new { id = id });
        }
        //get previous orders
        public async Task<IActionResult> MyOrders(int? id)
        {
            var bookdbContext = _context.OrderAuthors
                .Include(o => o.BookNavigation)
                .Include(o => o.AuthorNavigation)
                .Where(o => o.AuthorNavigation.Id == id)
                .Where(o => o.Purchased == 1);

            ViewData["ID"] = id;
            
            return View(await bookdbContext.ToListAsync());
        }

        // GET: create new order
        public IActionResult CreateOrder(int? id, int? idBook, string bookTitle)
        {
            ViewData["ID"] = id;
            ViewData["idBook"] = idBook;
            ViewData["BookTitle"] = bookTitle;
            ViewData["idOrder"] = _context.OrderAuthors.OrderBy(o => o.Id).Last().Id+1;
            ViewData["Book"] = new SelectList(_context.Books, "Id", "Title");
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Username");
            return View();
        }

        // POST: OrderCostumers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateOrder([Bind("Book,Author,Quantity,Id,Purchased")] OrderAuthor orderAuthor, int? userId)
        {
            if (ModelState.IsValid)
            {
                _context.Add(orderAuthor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Catalog), new { id = userId });
            }
            ViewData["Book"] = new SelectList(_context.Books, "Id", "Title", orderAuthor.Book);
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Username", orderAuthor.Author);
            return View(orderAuthor);
        }

        // GET: change how many books the author wants to buy
        public async Task<IActionResult> EditOrder(int? id, int? idBook, string bookTitle, int? userId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderAuthor = await _context.OrderAuthors.FindAsync(id);
            if (orderAuthor == null)
            {
                return NotFound();
            }

            ViewData["ID"] = userId;
            ViewData["idBook"] = idBook;
            ViewData["BookTitle"] = bookTitle;
            ViewData["Book"] = new SelectList(_context.Books, "Id", "Title", orderAuthor.Book);
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Email", orderAuthor.Author);
            return View(orderAuthor);
        }

        // POST: submit change to order
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditOrder(int id, [Bind("Book,Author,Quantity,Id,Purchased")] OrderAuthor orderAuthor, int? userId)
        {
            if (id != orderAuthor.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(orderAuthor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderAuthorExists(orderAuthor.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Catalog), new { id = userId });
            }
            ViewData["Book"] = new SelectList(_context.Books, "Id", "Title", orderAuthor.Book);
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Email", orderAuthor.Author);
            return View(orderAuthor);
        }

        // GET: order to delete
        public async Task<IActionResult> DeleteOrder(int? id, string bookTitle, int? userId)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewData["ID"] = userId;
            ViewData["BookTitle"] = bookTitle;
            var orderAuthor = await _context.OrderAuthors
                .Include(o => o.BookNavigation)
                .Include(o => o.AuthorNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (orderAuthor == null)
            {
                return NotFound();
            }

            return View(orderAuthor);
        }

        // POST: delete order from db
        [HttpPost, ActionName("DeleteOrder")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmedOrder(int id, int userId)
        {
            var orderAuthor = await _context.OrderAuthors.FindAsync(id);
            _context.OrderAuthors.Remove(orderAuthor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Catalog), new { id = userId });
        }

        private bool OrderAuthorExists(int id)
        {
            return _context.OrderAuthors.Any(e => e.Id == id);
        }

        // GET: info to create author
        public IActionResult Create()
        {
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email");
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1");
            return View();
        }

        // POST: submit new author to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Username,Email,Password,Adm,Catalog,Id")] Author author)
        {
            if (ModelState.IsValid)
            {
                _context.Add(author);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Catalog), new { id = author.Id });
            }
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email", author.Adm);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", author.Catalog);
            return View(author);
        }

        // GET: author to edit
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var author = await _context.Authors.FindAsync(id);
            if (author == null)
            {
                return NotFound();
            }
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email", author.Adm);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", author.Catalog);
            return View(author);
        }

        // POST: 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Username,Email,Password,Adm,Catalog,Id")] Author author)
        {
            if (id != author.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(author);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AuthorExists(author.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email", author.Adm);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", author.Catalog);
            return View(author);
        }

        // GET: Author to delete
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var author = await _context.Authors
                .Include(a => a.AdmNavigation)
                .Include(a => a.CatalogNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (author == null)
            {
                return NotFound();
            }

            return View(author);
        }

        // POST: delete author from db
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var author = await _context.Authors.FindAsync(id);
            _context.Authors.Remove(author);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AuthorExists(int id)
        {
            return _context.Authors.Any(e => e.Id == id);
        }
    }
}
