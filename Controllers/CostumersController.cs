﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Bookshop.Models;

namespace Bookshop.Controllers
{
    public class CostumersController : Controller
    {
        private readonly bookdbContext _context;

        public CostumersController(bookdbContext context)
        {
            _context = context;
        }

        // GET: authentification for costumers
        public async Task<IActionResult> Index(string username, string password)
        {
            var costumers = from m in _context.Costumers.Include(c => c.AdmNavigation).Include(c => c.CatalogNavigation)
                            select m;
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                costumers = costumers.Where(s => s.Username.Contains(username))
                    .Where(s => s.Password.Contains(password));
            }
            else return UserNotFound();
            if (!costumers.Any()) return UserNotFound();
            return View(await costumers.ToListAsync());
        }
        //get books from search or all the books
        public async Task<IActionResult> Catalog(int? id, string genre, string titleSearch, string author)
        {
            ViewData["ID"] = id;
            ViewData["titleSearch"] = titleSearch;
            ViewData["author"] = author;
            ViewData["genre"] = genre;
            var bookdbContext = from m in _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .OrderByDescending(book => book.RatingGoodreads)
                                select m;

            if (!String.IsNullOrEmpty(genre))
            {
                bookdbContext = bookdbContext.Where(s => s.GenreNavigation.Name.Contains(genre));
            }
            if (!String.IsNullOrEmpty(titleSearch))
            {
                bookdbContext = bookdbContext.Where(s => s.Title.Contains(titleSearch));
            }
            if (!String.IsNullOrEmpty(author))
            {
                bookdbContext = bookdbContext.Where(s => s.AuthorNavigation.Username.Contains(author));
            }

            return View(await bookdbContext.ToListAsync());
        }
        //get book details
        public async Task<IActionResult> DetailsBook(int? id, int? userId, int? rate)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            //costumers can vote for the book rating
            if (rate.HasValue)
            {
                if(book.Rating == null)
                {
                    book.Rating = rate;
                    book.Voted++;
                }
                else
                {
                    book.Rating = (book.Rating * book.Voted) + rate;
                    book.Voted++;
                    book.Rating /= book.Voted;
                }
                _context.Update(book);
                await _context.SaveChangesAsync();
            }
            if (book.Rating == null) ViewData["Rating"] = "not voted yet";
            else
            {
                var r = Convert.ToDouble(String.Format("{0:0.00}", book.Rating));
                ViewData["Rating"] = r;
            }
            if (book == null)
            {
                return NotFound();
            }
            ViewData["ID"] = userId;
            ViewData["bookId"] = id;
            return View(book);
        }

        public IActionResult UserNotFound()
        {
            return View("UserNotFound");
        }
        //get cart
        public async Task<IActionResult> Orders(int? id)
        {
            var bookdbContext = _context.OrderCostumers
                .Include(o => o.BookNavigation)
                .Include(o => o.CostumerNavigation)
                .Where(o => o.CostumerNavigation.Id == id)
                .Where(o => o.Purchased == 0);
            var books = bookdbContext.ToList();
            ViewData["ID"] = id;
            double totalPrice = 0; 
            //compute total price
            for (int i = 0; i < books.Count(); i++)
            {
                var order = books.ElementAt(i);
                totalPrice += (order.BookNavigation.Price * order.Quantity);
            }
            ViewData["Price"] = totalPrice;
            return View(await bookdbContext.ToListAsync());
        }
        //purchase books from cart, change book stock and add to myorders
        public async Task<IActionResult> Purchase(int? id)
        {
            var bookdbContext = _context.OrderCostumers
                .Include(o => o.BookNavigation)
                .Include(o => o.CostumerNavigation)
                .Where(o => o.CostumerNavigation.Id == id)
                .Where(o => o.Purchased == 0)
                .ToList();
            for(int i = 0; i < bookdbContext.Count(); i++)
            {
                var order = bookdbContext.ElementAt(i);
                order.Purchased = 1;
                _context.Update(order);
                var book = await _context.Books.FindAsync(order.BookNavigation.Id);
                book.Stock -= order.Quantity;
                _context.Update(book);
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Catalog), new { id = id });
        }
        //get provious orders
        public async Task<IActionResult> MyOrders(int? id)
        {
            var bookdbContext = _context.OrderCostumers
                .Include(o => o.BookNavigation)
                .Include(o => o.CostumerNavigation)
                .Where(o => o.CostumerNavigation.Id == id)
                .Where(o => o.Purchased == 1);

            ViewData["ID"] = id;
            return View(await bookdbContext.ToListAsync());
        }

        // GET: info to create order for book
        public IActionResult CreateOrder(int? id, int? idBook, string bookTitle)
        {
            ViewData["ID"] = id;
            ViewData["idBook"] = idBook;
            ViewData["BookTitle"] = bookTitle;
            ViewData["idOrder"] = _context.OrderCostumers.OrderBy(o => o.Id).Last().Id + 1;
            ViewData["Book"] = new SelectList(_context.Books, "Id", "Title");
            ViewData["Costumer"] = new SelectList(_context.Costumers, "Id", "Username");
            return View();
        }

        // POST: submit new order to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateOrder([Bind("Book,Costumer,Quantity,Id,Purchased")] OrderCostumer orderCostumer, int? userId)
        {
            if (ModelState.IsValid)
            {
                _context.Add(orderCostumer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Catalog), new { id = userId });
            }
            ViewData["Book"] = new SelectList(_context.Books, "Id", "Title", orderCostumer.Book);
            ViewData["Costumer"] = new SelectList(_context.Costumers, "Id", "Username", orderCostumer.Costumer);
            return View(orderCostumer);
        }

        // GET: order to edit
        public async Task<IActionResult> EditOrder(int? id, int? idBook, string bookTitle, int? userId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orderCostumer = await _context.OrderCostumers.FindAsync(id);
            if (orderCostumer == null)
            {
                return NotFound();
            }

            ViewData["ID"] = userId;
            ViewData["idBook"] = idBook;
            ViewData["BookTitle"] = bookTitle;
            ViewData["Book"] = new SelectList(_context.Books, "Id", "Title", orderCostumer.Book);
            ViewData["Costumer"] = new SelectList(_context.Costumers, "Id", "Email", orderCostumer.Costumer);
            return View(orderCostumer);
        }

        // POST: submit changed order to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditOrder(int id, [Bind("Book,Costumer,Quantity,Id,Purchased")] OrderCostumer orderCostumer, int? userId)
        {
            if (id != orderCostumer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(orderCostumer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderCostumerExists(orderCostumer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Catalog), new { id = userId });
            }
            ViewData["Book"] = new SelectList(_context.Books, "Id", "Title", orderCostumer.Book);
            ViewData["Costumer"] = new SelectList(_context.Costumers, "Id", "Email", orderCostumer.Costumer);
            return View(orderCostumer);
        }

        // GET: order to delete
        public async Task<IActionResult> DeleteOrder(int? id, string bookTitle, int? userId)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewData["ID"] = userId;
            ViewData["BookTitle"] = bookTitle;
            var orderCostumer = await _context.OrderCostumers
                .Include(o => o.BookNavigation)
                .Include(o => o.CostumerNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (orderCostumer == null)
            {
                return NotFound();
            }

            return View(orderCostumer);
        }

        // POST: delete order from db
        [HttpPost, ActionName("DeleteOrder")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmedOrder(int id, int userId)
        {
            var orderCostumer = await _context.OrderCostumers.FindAsync(id);
            _context.OrderCostumers.Remove(orderCostumer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Catalog), new { id = userId });
        }

        private bool OrderCostumerExists(int id)
        {
            return _context.OrderCostumers.Any(e => e.Id == id);
        }

        // GET: get costumer details
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var costumer = await _context.Costumers
                .Include(c => c.AdmNavigation)
                .Include(c => c.CatalogNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (costumer == null)
            {
                return NotFound();
            }

            return View(costumer);
        }

        // GET: create new costumer
        public IActionResult Create()
        {
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email");
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1");
            return View();
        }

        // POST: add new costumer to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Username,Email,Password,Adm,Catalog,Id")] Costumer costumer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(costumer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Catalog), new { id = costumer.Id });
            }
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email", costumer.Adm);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", costumer.Catalog);
            return View(costumer);
        }

        // GET: get costumer to edit
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var costumer = await _context.Costumers.FindAsync(id);
            if (costumer == null)
            {
                return NotFound();
            }
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email", costumer.Adm);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", costumer.Catalog);
            return View(costumer);
        }

        // POST: submit changed costumer
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Username,Email,Password,Adm,Catalog,Id")] Costumer costumer)
        {
            if (id != costumer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(costumer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CostumerExists(costumer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email", costumer.Adm);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", costumer.Catalog);
            return View(costumer);
        }

        // GET: costumer to delete
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var costumer = await _context.Costumers
                .Include(c => c.AdmNavigation)
                .Include(c => c.CatalogNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (costumer == null)
            {
                return NotFound();
            }

            return View(costumer);
        }

        // POST: delete costumer from db
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var costumer = await _context.Costumers.FindAsync(id);
            _context.Costumers.Remove(costumer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CostumerExists(int id)
        {
            return _context.Costumers.Any(e => e.Id == id);
        }
    }
}
