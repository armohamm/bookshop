﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Bookshop.Models;

namespace Bookshop.Controllers
{
    public class AdministratorsController : Controller
    {
        private readonly bookdbContext _context;

        public AdministratorsController(bookdbContext context)
        {
            _context = context;
        }

        // GET: Administrators
        public async Task<IActionResult> Index(string username, string password)
        {
            var administrators = from m in _context.Administrators.Include(a => a.CatalogNavigation)
                          select m;
            //authentification for administrator
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                administrators = administrators.Where(s => s.Username.Contains(username))
                    .Where(s => s.Password.Contains(password));
            }
            else return UserNotFound();
            if (!administrators.Any()) return UserNotFound();
            return View(await administrators.ToListAsync());
        }
        //return Catalog view with all the books, or if we searched for something, all 
        //the books that matched
        public async Task<IActionResult> Catalog(string genre, string titleSearch, string author)
        {
            ViewData["titleSearch"] = titleSearch;
            ViewData["author"] = author;
            ViewData["genre"] = genre;
            var bookdbContext = from m in _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .OrderByDescending(book => book.RatingGoodreads)
                                select m;

            if (!String.IsNullOrEmpty(genre))
            {
                bookdbContext = bookdbContext.Where(s => s.GenreNavigation.Name.Contains(genre));
            }
            if (!String.IsNullOrEmpty(titleSearch))
            {
                bookdbContext = bookdbContext.Where(s => s.Title.Contains(titleSearch));
            }
            if (!String.IsNullOrEmpty(author))
            {
                bookdbContext = bookdbContext.Where(s => s.AuthorNavigation.Username.Contains(author));
            }

            return View(await bookdbContext.ToListAsync());
        }
        //get all authors in the db
        public async Task<IActionResult> Authors()
        {
            var bookdbContext = _context.Authors.Include(a => a.AdmNavigation).Include(a => a.CatalogNavigation);
            return View(await bookdbContext.ToListAsync());
        }
        //get all the publishers in the db
        public async Task<IActionResult> Publishers()
        {
            var bookdbContext = _context.Publishers.Include(p => p.AdmNavigation).Include(p => p.CatalogNavigation);
            return View(await bookdbContext.ToListAsync());
        }
        //get all the costumers from the db
        public async Task<IActionResult> Costumers()
        {
            var bookdbContext = _context.Costumers.Include(c => c.AdmNavigation).Include(c => c.CatalogNavigation);
            return View(await bookdbContext.ToListAsync());
        }
        //get all the genres from the db
        public async Task<IActionResult> IndexGenre()
        {
            var bookdbContext = _context.Genres;
            return View(await bookdbContext.ToListAsync());
        }
        //get: edit a specific genre
        public async Task<IActionResult> EditGenre(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var g = await _context.Genres.FindAsync(id);
            if (g == null)
            {
                return NotFound();
            }
            
            return View(g);
        }

        // post: submit changes to db and notify all observer books that the name changed
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditGenre(int id, [Bind("Name, Id")] Genre genre)
        {
            if (id != genre.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(genre);
                    await _context.SaveChangesAsync();
                    genre.Notify(_context.Books.Where(book => book.Genre == id));
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(genre.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(IndexGenre));
            }
           
            return View(genre);
        }
        //failed login
        public IActionResult UserNotFound()
        {
            return View("UserNotFound");
        }

        // get: all the fields to create the book
        public IActionResult CreateBook()
        {
            ViewData["idBook"] = _context.Books.OrderBy(b => b.Id).Last().Id + 1;
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Username");
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1");
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name");
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Username");
            return View();
        }

        //post: submit new book to the db and add it to the genre it observes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateBook([Bind("Id,Title,CoverImage,Summary,Price,RatingGoodreads,Rating,Genre,Author,Publisher,Year,Catalog,Stock,Voted")] Book book)
        {
            if (ModelState.IsValid)
            {
                _context.Add(book);
                await _context.SaveChangesAsync();
                var gen = from m in _context.Genres.Where(genre => genre.Id == book.Genre) select m;
                foreach (var g in gen)
                {
                    g.Attach(book);///
                }
                return RedirectToAction(nameof(Catalog));
            }
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Username", book.Author);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", book.Catalog);
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name", book.Genre);
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Username", book.Publisher);
            return View(book);
        }
        //get: book to edit
        public async Task<IActionResult> EditBook(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }

            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Username", book.Author);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", book.Catalog);
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name", book.Genre);
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Username", book.Publisher);
            return View(book);
        }

        //post: submit the book with the changes and attach if neccessary to new genre
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBook(int id, [Bind("Id,Title,CoverImage,Summary,Price,RatingGoodreads,Rating,Genre,Author,Publisher,Year,Catalog,Stock,Voted")] Book book)
        {
            if (id != book.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                    var gen = from m in _context.Genres.Where(genre => genre.Id == book.Genre) select m;
                    foreach (var g in gen)
                    {
                        g.Attach(book);///
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Catalog));
            }
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Username", book.Author);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", book.Catalog);
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name", book.Genre);
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Username", book.Publisher);
            return View(book);
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.Id == id);
        }
        //get: book details based on id
        public async Task<IActionResult> DetailsBook(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            //compute rating based on how many people voted and return the needed format
            if (book.Rating == null) ViewData["Rating"] = "not voted yet";
            else
            {
                var r = Convert.ToDouble(String.Format("{0:0.00}", book.Rating));
                ViewData["Rating"] = r;
            }
            if (book == null)
            {
                return NotFound();
            }
            return View(book);
        }

        // GET: book to be deleted
        public async Task<IActionResult> DeleteBook(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }
            //show the required format for rating
            if (book.Rating == null) ViewData["Rating"] = "not voted yet";
            else
            {
                var r = Convert.ToDouble(String.Format("{0:0.00}", book.Rating));
                ViewData["Rating"] = r;
            }
            return View(book);
        }

        //post: delete book from db and detach it from genre
        [HttpPost, ActionName("DeleteBook")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmedBook(int id)
        {
            var book = await _context.Books.FindAsync(id);
            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            var gen = from m in _context.Genres.Where(genre => genre.Id == book.Genre) select m;
            foreach (var g in gen)
            {
                g.Detach(book);///
            }
            return RedirectToAction(nameof(Catalog));
        }

        //get administrator details
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var administrator = await _context.Administrators
                .Include(a => a.CatalogNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (administrator == null)
            {
                return NotFound();
            }

            return View(administrator);
        }

        // GET: create administrator
        public IActionResult Create()
        {
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1");
            return View();
        }

        // POST:submit changes to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Username,Email,Password,Catalog,Id")] Administrator administrator)
        {
            if (ModelState.IsValid)
            {
                _context.Add(administrator);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Catalog));
            }
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", administrator.Catalog);
            return View(administrator);
        }

        // GET: get administrator to edit
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var administrator = await _context.Administrators.FindAsync(id);
            if (administrator == null)
            {
                return NotFound();
            }
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", administrator.Catalog);
            return View(administrator);
        }

        // POST: submit changed admin to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Username,Email,Password,Catalog,Id")] Administrator administrator)
        {
            if (id != administrator.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(administrator);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdministratorExists(administrator.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Catalog));
            }
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", administrator.Catalog);
            return View(administrator);
        }

        // GET: administrator to be erased
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var administrator = await _context.Administrators
                .Include(a => a.CatalogNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (administrator == null)
            {
                return NotFound();
            }

            return View(administrator);
        }

        // POST: delete admin from db
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var administrator = await _context.Administrators.FindAsync(id);
            _context.Administrators.Remove(administrator);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Catalog));
        }

        private bool AdministratorExists(int id)
        {
            return _context.Administrators.Any(e => e.Id == id);
        }
    }
}
