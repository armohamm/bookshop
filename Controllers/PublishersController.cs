﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Bookshop.Models;

namespace Bookshop.Controllers
{
    public class PublishersController : Controller
    {
        private readonly bookdbContext _context;

        public PublishersController(bookdbContext context)
        {
            _context = context;
        }

        // GET: authentification for publisher
        public async Task<IActionResult> Index(string username, string password)
        {
            var publishers = from m in _context.Publishers.Include(p => p.AdmNavigation).Include(p => p.CatalogNavigation)
                             select m;
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                publishers = publishers.Where(s => s.Username.Contains(username))
                    .Where(s => s.Password.Contains(password));
            }
            else return UserNotFound();
            if (!publishers.Any()) return UserNotFound();
            return View(await publishers.ToListAsync());
        }

        public IActionResult UserNotFound()
        {
            return View("UserNotFound");
        }
        //get books searched or all the books
        public async Task<IActionResult> Catalog(int? id, string genre, string titleSearch, string author)
        {
            ViewData["ID"] = id;
            ViewData["titleSearch"] = titleSearch;
            ViewData["author"] = author;
            ViewData["genre"] = genre;
            var bookdbContext = from m in _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .OrderByDescending(book => book.RatingGoodreads)
                                select m;

            if (!String.IsNullOrEmpty(genre))
            {
                bookdbContext = bookdbContext.Where(s => s.GenreNavigation.Name.Contains(genre));
            }
            if (!String.IsNullOrEmpty(titleSearch))
            {
                bookdbContext = bookdbContext.Where(s => s.Title.Contains(titleSearch));
            }
            if (!String.IsNullOrEmpty(author))
            {
                bookdbContext = bookdbContext.Where(s => s.AuthorNavigation.Username.Contains(author));
            }

            return View(await bookdbContext.ToListAsync());
        }
        //get books that belong to the publisher
        public async Task<IActionResult> MyBooks(int? id)
        {
            var books = from m in _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Where(b => b.PublisherNavigation.Id == id) select m;

            ViewData["ID"] = id;
            return View(await books.OrderByDescending(book => book.RatingGoodreads).ToListAsync());
        }
        //get info to create new book
        public IActionResult CreateBook(int? id)
        {
            ViewData["ID"] = id;
            ViewData["idBook"] = _context.Books.OrderBy(book => book.Id).Last().Id + 1;
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Username");
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1");
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name");
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Username");
            return View();
        }

        // POST: add book to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateBook([Bind("Id,Title,CoverImage,Summary,Price,RatingGoodreads,Rating,Genre,Author,Publisher,Year,Catalog,Stock,Voted")] Book book, int? userId)
        {
            if (ModelState.IsValid)
            {
 
                
                _context.Add(book);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Catalog), new { id = userId });
            }
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Email", book.Author);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", book.Catalog);
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name", book.Genre);
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Email", book.Publisher);
            return View(book);
        }
        //get book to edit
        public async Task<IActionResult> EditBook(int? id, int? userId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }
            ViewData["ID"] = userId;

            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Email", book.Author);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", book.Catalog);
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name", book.Genre);
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Email", book.Publisher);
            return View(book);
        }

        // POST: submit changes to book
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBook(int id, [Bind("Id,Title,CoverImage,Summary,Price,RatingGoodreads,Rating,Genre,Author,Publisher,Year,Catalog,Stock,Voted")] Book book, int? userId)
        {
            if (id != book.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Catalog), new { id = userId });
            }
            ViewData["Author"] = new SelectList(_context.Authors, "Id", "Email", book.Author);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", book.Catalog);
            ViewData["Genre"] = new SelectList(_context.Genres, "Id", "Name", book.Genre);
            ViewData["Publisher"] = new SelectList(_context.Publishers, "Id", "Email", book.Publisher);
            return View(book);
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.Id == id);
        }

        // GET: book details
        public async Task<IActionResult> DetailsBook(int? id, int? userId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }
            if (book.Rating == null) ViewData["Rating"] = "not voted yet";
            else
            {
                var r = Convert.ToDouble(String.Format("{0:0.00}", book.Rating));
                ViewData["Rating"] = r;
            }
            ViewData["ID"] = userId;
            return View(book);
        }

        // GET: book to be deleted
        public async Task<IActionResult> DeleteBook(int? id, int? userId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.AuthorNavigation)
                .Include(b => b.CatalogNavigation)
                .Include(b => b.GenreNavigation)
                .Include(b => b.PublisherNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }
            if (book.Rating == null) ViewData["Rating"] = "not voted yet";
            else
            {
                var r = Convert.ToDouble(String.Format("{0:0.00}", book.Rating));
                ViewData["Rating"] = r;
            }
            ViewData["ID"] = userId;
            return View(book);
        }

        // POST: delete book from db
        [HttpPost, ActionName("DeleteBook")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmedBook(int id, int? userId)
        {
            var book = await _context.Books.FindAsync(id);
            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Catalog), new { id = userId });
        }

        // GET: publisher details
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publisher = await _context.Publishers
                .Include(p => p.AdmNavigation)
                .Include(p => p.CatalogNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (publisher == null)
            {
                return NotFound();
            }

            return View(publisher);
        }

        // GET: create new publisher
        public IActionResult Create()
        {
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email");
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1");
            return View();
        }

        // POST: 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Username,Email,Password,Adm,Catalog,Id")] Publisher publisher)
        {
            if (ModelState.IsValid)
            {
                _context.Add(publisher);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Catalog), new { id = publisher.Id });
            }
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email", publisher.Adm);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", publisher.Catalog);
            return View(publisher);
        }

        // GET: publisher to edit
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publisher = await _context.Publishers.FindAsync(id);
            if (publisher == null)
            {
                return NotFound();
            }
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email", publisher.Adm);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", publisher.Catalog);
            return View(publisher);
        }

        // POST: add publisher to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Username,Email,Password,Adm,Catalog,Id")] Publisher publisher)
        {
            if (id != publisher.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(publisher);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PublisherExists(publisher.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Adm"] = new SelectList(_context.Administrators, "Id", "Email", publisher.Adm);
            ViewData["Catalog"] = new SelectList(_context.Catalogs, "Catalog1", "Catalog1", publisher.Catalog);
            return View(publisher);
        }

        // GET: Publisher to delete
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publisher = await _context.Publishers
                .Include(p => p.AdmNavigation)
                .Include(p => p.CatalogNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (publisher == null)
            {
                return NotFound();
            }

            return View(publisher);
        }

        // POST: delete publisher from db
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var publisher = await _context.Publishers.FindAsync(id);
            _context.Publishers.Remove(publisher);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PublisherExists(int id)
        {
            return _context.Publishers.Any(e => e.Id == id);
        }
    }
}
